import pygame, sys
from setting import *

class Game:
    #Partie initialisation des variable pour l'utilisation dans le fichier
    def __init__(self):
        #Configuration de base initialisatin de pygame
        pygame.init()
        #initialisation de l'écran
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        #initialisation du timer
        self.clock = pygame.time.Clock()

    # Méthode de lancement du jeu
    def run(self):
        #tant que True le jeu est lancé
        while True:
            #vérification si on clique sur la croix pour fermer le jeu
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            #remplir l'écran avec un fond noir
            self.screen.fill('black')
            #mis à jours l'affichage
            pygame.display.update()
            #check du FPS
            self.clock.tick(FPS)

#vérification si on lance le fichier on appelle le run
if __name__ == '__main__':
    game = Game()
    game.run()
